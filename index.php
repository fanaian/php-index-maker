<?php

/**
 * @param $bytes
 * @return float|int|string
 */
function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "TB",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "GB",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "MB",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "KB",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "B",
            "VALUE" => 1
        ),
    );
    $result = 0;
    foreach ($arBytes as $arItem) {
        if ($bytes >= $arItem["VALUE"]) {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", ",", strval(round($result, 2))) . " " . $arItem["UNIT"];
            break;
        }
    }
    return $result;
}

$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$allFiles = scandir(__DIR__); // Or any other directory
$files = array_diff($allFiles, ['.', '..', '.gitignore', '.git', 'index.php']);
foreach ($files as $x) {
    $filePath = __DIR__ . '/' . $x;
    $fileSize = FileSizeConvert(fileSize($filePath));
    $fileCreatedAt = date("Y-m-d H:i:s.", filemtime($filePath));
    echo "<a href='$link$x'>" . $x . "</a>" . "&nbsp;&nbsp;&nbsp;&nbsp;" . $fileSize . "&nbsp;&nbsp;&nbsp;&nbsp;" . $fileCreatedAt . "<br>";
}
